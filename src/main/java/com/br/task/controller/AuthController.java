package com.br.task.controller;

import com.br.task.dto.AccessTokenDto;
import com.br.task.dto.SignInDto;
import com.br.task.dto.SignUpDto;
import com.br.task.model.User;
import com.br.task.reposiroty.UserRepository;
import com.br.task.security.JwtService;
import com.br.task.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;
import java.util.List;

//@Slf4j
//@RestController
//@RequestMapping("/users")
//@RequiredArgsConstructor
//public class UserController {
//    private  final UserService userService;
//
//    @GetMapping
//    public List<User> getAllUser() {
//        return userService.getAllUser();
//    }
//
//}


@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);

    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;
    private  final UserRepository userRepository;





    @PostMapping("/sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@Valid @RequestBody SignInDto signInDto) {//ResponseEntity<AccessTokenDto>
        log.trace("Login request by user {}", signInDto.getEmail());

        log.trace("Authenticating user {}", signInDto.getEmail());

        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(signInDto.getEmail(), signInDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Duration duration = getDuration(signInDto.getRememberMe());
        String jwt =jwtService.issueToken(authentication, duration);
        HttpHeaders httpHeaders;
        httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new AccessTokenDto(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Validated SignUpDto dto) {
        log.trace("Sign up request with email {}", dto.getEmail());
        userService.signUp(dto);
        return ResponseEntity.ok().build();
    }

    private Duration getDuration(Boolean rememberMe) {
        return ((rememberMe != null) && rememberMe) ? ONE_WEEK : ONE_DAY;
    }
}

