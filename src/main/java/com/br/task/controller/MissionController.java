package com.br.task.controller;

import com.br.task.dto.MissionRequestDto;
import com.br.task.dto.MissionResponseDto;
import com.br.task.service.MissionServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/mission")
@RequiredArgsConstructor
public class MissionController {

    private final MissionServiceImpl missionService;

//    @GetMapping("/{student_user_id}")
//    public MissionResponseDto getTaskByAssigedUserId(@PathVariable("id") Long id) {
//        return missionService.getMissionForStudent(id);
//    }

    @GetMapping("/{id}")
    public MissionResponseDto getTaskById(@PathVariable("id") Long id) {
        return missionService.getMissionById(id);
    }

    @PostMapping()
    public ResponseEntity<Void> createEmployee(@RequestBody @Valid MissionRequestDto taskDto )  {
        missionService.saveMission(taskDto);
        System.out.println(taskDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


}
