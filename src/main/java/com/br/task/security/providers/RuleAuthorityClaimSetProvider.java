package com.br.task.security.providers;


//import az.tezbazar.usermanagmentms.security.service.ClaimSet;
//import az.tezbazar.usermanagmentms.security.service.ClaimSetProvider;
import com.br.task.security.service.ClaimSet;
import com.br.task.security.service.ClaimSetProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RuleAuthorityClaimSetProvider implements ClaimSetProvider {

    private static final String RULE = "rule";

    @Override
    public ClaimSet provide(Authentication authentication) {
        log.trace("Providing claims");

        Set<String> authorities = authentication.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toSet());
        System.out.println(authorities);
        return new ClaimSet(RULE, authorities);
    }
}
