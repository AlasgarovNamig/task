package com.br.task.reposiroty;

import com.br.task.model.Mission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MissionRepository extends JpaRepository<Mission,Long> {
//    Optional<Mission> findByAssignedUserId(Long id);
    Optional<Mission> findByAssignedUserId(Long id);


}
