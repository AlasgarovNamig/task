package com.br.task.dto;

import com.br.task.model.MissionStatus;

import java.time.LocalDateTime;

public class MissionResponseDto {
    private String description;
    private LocalDateTime creationDate;
    private LocalDateTime deadline_date;
    private Long created_user_id;
    private Long assigned_user_id;
    private MissionStatus status;
}
