package com.br.task.dto;

import com.br.task.model.MissionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MissionRequestDto {

    private String description;
    private String deadlineDate;
    private Long createdUserId;
    private Long assignedUserId;
}
