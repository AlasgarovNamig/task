package com.br.task.service;


import com.br.task.dto.SignUpDto;
import com.br.task.model.User;

import java.util.List;

public interface UserService {
    void signUp(SignUpDto signUpDto);

}
