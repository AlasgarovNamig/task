package com.br.task.service;

import com.br.task.dto.MissionRequestDto;
import com.br.task.dto.MissionResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface MissionService {
//    public MissionResponseDto getMissionForStudent(Long id);
    public MissionResponseDto getMissionById(Long id);
    public void saveMission(MissionRequestDto missionRequestDtoDto);
}
