package com.br.task.service;

import com.br.task.dto.MissionRequestDto;
import com.br.task.dto.MissionResponseDto;
import com.br.task.model.Mission;
import com.br.task.reposiroty.MissionRepository;
import com.br.task.reposiroty.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MissionServiceImpl implements MissionService {
    private final MissionRepository missionRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper;

//    @Override
//    public MissionResponseDto getMissionForStudent(Long id){
//      return   missionRepository.findByAssignedUserId(id).map(task -> mapper.map(task, MissionResponseDto.class))
////                .orElseThrow(GivenTask.az.GivenTask.exception.NotFoundException::new);
//                .orElseThrow(RuntimeException::new);
//    }
    @Override
    public MissionResponseDto getMissionById(Long id) {
        return   missionRepository.findById(id).map(task -> mapper.map(task, MissionResponseDto.class))
//                .orElseThrow(GivenTask.az.GivenTask.exception.NotFoundException::new);
                .orElseThrow(RuntimeException::new);

    }

    @Override
    public void saveMission(MissionRequestDto taskDto) {
        Mission mission = new Mission();
        mission.setDescription(taskDto.getDescription());
        mission.setDeadlineDate(LocalDateTime.parse(taskDto.getDeadlineDate()));
        mission.setCreatedUserId(userRepository.getById(taskDto.getCreatedUserId()));
        mission.setAssignedUserId(userRepository.getById(taskDto.getAssignedUserId()));

        missionRepository.save(mission);

    }
}
