package com.br.task.service;


import com.br.task.dto.SignUpDto;
import com.br.task.model.Role;
import com.br.task.model.User;
import com.br.task.reposiroty.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService{//, UserDetailsService

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUp(SignUpDto signUpDto) {
        userRepository.findByEmail(signUpDto.getEmail())
                .ifPresent(user -> {
                    throw  new RuntimeException();
                    //throw new EmailAlreadyUsedException(dto.getEmail());
                });
        User user = createUserEntityObject(signUpDto);
        userRepository.save(user);

    }

    private  User  createUserEntityObject(SignUpDto signUpDto){
        User user = new User();
        user.setEmail(signUpDto.getEmail());
        user.setUsername(signUpDto.getName());
        user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
      User user=  userRepository.findByEmail(email).get();
      List<SimpleGrantedAuthority> authorities = new ArrayList<>();
      authorities.add(new SimpleGrantedAuthority(user.getRole().toString()));
      return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),authorities) ;

    }

}
